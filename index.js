var Service, Characteristic;
var request = require('request-promise');

module.exports = function(homebridge){
    Service = homebridge.hap.Service;
    Characteristic = homebridge.hap.Characteristic;
    homebridge.registerAccessory('homebridge-octoprint', 'OctoPrint', OctoPrint);
}

function P0(fn) { return async cb => { try { cb(null, await fn()) } catch (ex) { cb(ex) } } }
function P1(fn) { return async (v, cb) => { try { cb(null, await fn(v)) } catch (ex) { cb(ex) } } }

class OctoPrint {
    constructor(log, config) {
        const {
            name = 'OctoPrint',
            api_key: apiKey,
            server = 'http://octopi.local'
        } = config

        const api = new API({ server, apiKey, log })
        Object.assign(this, { name, api, log })

        this.status = new Status({ name, api, log })
        this.bedHeater = new BedHeater({ name, api, log })
        this.toolHeater = new ToolHeater({ name, api, log })

        log.info(`[OCTOPRINT] Initialized Accessory at '${server}'`)
    }

    getServices() {
        this.log('[OCTOPRINT] Initializing accessory information')
        const info = new Service.AccessoryInformation()
        info.setCharacteristic(Characteristic.Manufacturer, 'Ethan Reesor et al')
        info.setCharacteristic(Characteristic.Model, 'OctoPrint')
        info.setCharacteristic(Characteristic.SerialNumber, '0001')
        info.setCharacteristic(Characteristic.FirmwareRevision, '0001')
        info.setCharacteristic(Characteristic.Name, this.name)

        return [
            info,
            ...this.status.getServices(),
            ...this.bedHeater.getServices(),
            ...this.toolHeater.getServices(),
        ]
    }
}

class API {
    constructor({ server, apiKey, log }) {
        if (server.endsWith('/'))
            server = server.substring(0, server.length - 1)
        Object.assign(this, { server, apiKey, log })
    }

    get headers() {
        return { 'X-Api-Key': this.apiKey }
    }

    request(method, path, options = { json: true }, body = null) {
        if (path.startsWith('/'))
            path = path.substring(1)

        const { log, server, headers } = this
        const uri = `${server}/api/${path}`

        log.info(`[OCTOPRINT] ${method} ${uri}`)
        return request(Object.assign({ method, uri, headers, body }, options)).catch(x => {
            log.error(`[OCTOPRINT] Failed to ${method} ${path}: ${x}`)
            throw x
        })
    }

    get(path, options = { json: true }) {
        return this.request('GET', path, options)
    }

    post(path, body, options = { json: true }) {
        return this.request('POST', path, options, body)
    }

    put(path, body, options = { json: true }) {
        return this.request('PUT', path, options, body)
    }
}

class Status {
    constructor({ name, api, log }) {
        Object.assign(this, { name, api, log })
    }

    getServices() {
        this.log('[OCTOPRINT] Initializing status service')

        const service = new Service.Lightbulb(`${this.name} Status`, 'job')

        service.getCharacteristic(Characteristic.On)
            .on('get', P0(() => this.getState()))
            .on('set', P1(x => this.setState(x)))

        service.getCharacteristic(Characteristic.Brightness)
            .on('get', P0(() => this.getProgress()))
            .on('set', P1(x => this.setProgress(x)))

        const battery = new Service.BatteryService()
        battery.setCharacteristic(Characteristic.Name, `${this.name} Status B`)

        battery.getCharacteristic(Characteristic.BatteryLevel)
            .on('get', P0(() => this.getProgress()))
            .on('set', P1(x => this.setProgress(x)))

        battery.getCharacteristic(Characteristic.ChargingState)
            .on('get', P0(() => this.getState() ? Characteristic.ChargingState.CHARGING : Characteristic.ChargingState.NOT_CHARGING))

        battery.getCharacteristic(Characteristic.StatusLowBattery)
            .on('get', P0(() => Characteristic.StatusLowBattery.BATTERY_LEVEL_NORMAL))

        Object.assign(this, { service })
        return [service, battery]
    }

    async getState() {
        this.log(`[OCTOPRINT] Fetching current printing state`)

        const printState = await this.api.get('printer')
        const state = printState.state.flags.printing
        return state ? 1 : 0
    }

    async setState(value) {
        return this.service.getCharacteristic(Characteristic.On).getValue()
    }

    async getProgress() {
        this.log('[OCTOPRINT] Fetching current job data')

        const printState = await this.api.get('job')
        const completion = printState.progress.completion

        if (!completion) {
            this.log('[OCTOPRINT] Printer currently not printing')
            return 0
        }

        this.log(`[OCTOPRINT] Current print job at ${completion}`)
        return Math.round(parseFloat(completion))
    }

    async setProgress() {
        return this.service.getCharacteristic(Characteristic.Brightness).getValue()
    }
}

class Heater {
    constructor({ name, api, log }) {
        Object.assign(this, { name, api, log })
    }

    createThermostat(name, subtype) {
        const service = new Service.Thermostat(name, subtype)

        service.getCharacteristic(Characteristic.CurrentHeatingCoolingState)
            .on('get', P0(() => this.getCurrentHeatingCoolingState()))

        service.getCharacteristic(Characteristic.TargetHeatingCoolingState)
            .on('get', P0(() => this.getTargetHeatingCoolingState()))
            .on('set', P1(x => this.setTargetHeatingCoolingState(x)))

        service.getCharacteristic(Characteristic.CurrentTemperature)
            .on('get', P0(() => this.getCurrentTemperature()))

        service.getCharacteristic(Characteristic.TargetTemperature)
            .on('get', P0(() => this.getTargetTemperature()))
            .on('set', P1(x => this.getTargetTemperature()))

        service.getCharacteristic(Characteristic.TemperatureDisplayUnits)
            .on('get', P0(() => Characteristic.TemperatureDisplayUnits.CELSIUS))
            .on('set', P1(x => Characteristic.TemperatureDisplayUnits.CELSIUS))

        return service
    }
}

class BedHeater extends Heater {
    getServices() {
        this.log('[OCTOPRINT] Initializing bed heater service')
        const service = this.createThermostat()
        Object.assign(this, { service })
        return [service]
    }

    async getCurrentHeatingCoolingState() {
        this.log('[OCTOPRINT] Fetching current bed heater state')
        const { HEAT, OFF } = Characteristic.CurrentHeatingCoolingState
        const printer = await this.api.get('printer')
        return printer.temperature.bed.target ? HEAT : OFF
    }

    async getTargetHeatingCoolingState() {
        this.log('[OCTOPRINT] Fetching target bed heater state')
        const { HEAT, OFF } = Characteristic.TargetHeatingCoolingState
        const printer = await this.api.get('printer')
        return printer.temperature.bed.target ? HEAT : OFF
    }

    async setTargetHeatingCoolingState(value) {
        const { AUTO, COOL, HEAT, OFF } = Characteristic.TargetHeatingCoolingState
        const printer = await this.api.get('printer')

        if (printer.state.flags.printing) {
            this.log('[OCTOPRINT] Printing, not changing temperature')
            return printer.temperature.bed.target ? HEAT : OFF
        }

        switch (value) {
        case AUTO: case HEAT:
            this.log('[OCTOPRINT] Setting bed heater for PLA')
            await this.api.post('printer/bed', { command: 'target', target: 60 })
            this.service.getCharacteristic(Characteristic.TargetTemperature).setValue(60)
            return HEAT

        case OFF: case COOL:
            this.log('[OCTOPRINT] Turning off bed heater')
            await this.api.post('printer/bed', { command: 'target', target: 0 })
            this.service.getCharacteristic(Characteristic.TargetTemperature).setValue(0)
            return OFF
        }
    }

    async getCurrentTemperature() {
        this.log('[OCTOPRINT] Fetching current bed temperature')
        const printer = await this.api.get('printer')
        return printer.temperature.bed.actual
    }

    async getTargetTemperature() {
        this.log('[OCTOPRINT] Fetching target bed temperature')
        const printer = await this.api.get('printer')
        return printer.temperature.bed.target
    }
}

class ToolHeater extends Heater {
    getServices() {
        this.log('[OCTOPRINT] Initializing extruder heater service')
        const service = this.createThermostat(`${this.name} Extruder Heater`, 'tool0')
        Object.assign(this, { service })
        return [service]
    }

    async getCurrentHeatingCoolingState() {
        this.log('[OCTOPRINT] Fetching current extruder heater state')
        const { HEAT, OFF } = Characteristic.CurrentHeatingCoolingState
        const printer = await this.api.get('printer')
        return printer.temperature.tool0.target ? HEAT : OFF
    }

    async getTargetHeatingCoolingState() {
        this.log('[OCTOPRINT] Fetching target extruder heater state')
        const { HEAT, OFF } = Characteristic.TargetHeatingCoolingState
        const printer = await this.api.get('printer')
        return printer.temperature.tool0.target ? HEAT : OFF
    }

    async setTargetHeatingCoolingState(value) {
        const { AUTO, COOL, HEAT, OFF } = Characteristic.TargetHeatingCoolingState
        const printer = await this.api.get('printer')

        if (printer.state.flags.printing) {
            this.log('[OCTOPRINT] Printing, not changing temperature')
            return printer.temperature.tool0.target ? HEAT : OFF
        }

        switch (value) {
        case AUTO: case HEAT:
            this.log('[OCTOPRINT] Setting extruder heater for PLA')
            await this.api.post('printer/tool', { command: 'target', targets: { tool0: 215 } })
            this.service.getCharacteristic(Characteristic.TargetTemperature).setValue(215)

            return HEAT

        case OFF: case COOL:
            this.log('[OCTOPRINT] Turning off extruder heater')
            await this.api.post('printer/tool', { command: 'target', targets: { tool0: 0 } })
            this.service.getCharacteristic(Characteristic.TargetTemperature).setValue(0)
            return OFF
        }
    }

    async getCurrentTemperature() {
        this.log('[OCTOPRINT] Fetching current extruder temperature')
        const printer = await this.api.get('printer')
        return printer.temperature.tool0.actual
    }

    async getTargetTemperature() {
        this.log('[OCTOPRINT] Fetching target extruder temperature')
        const printer = await this.api.get('printer')
        return printer.temperature.tool0.target
    }
}

// {
//     sd: { ready: true },
//     state:
//     {
//         flags:
//         {
//             cancelling: false,
//             closedOrError: false,
//             error: false,
//             finishing: false,
//             operational: true,
//             paused: false,
//             pausing: false,
//             printing: false,
//             ready: true,
//             resuming: false,
//             sdReady: true
//         },
//         text: 'Operational'
//     },
//     temperature:
//     {
//         bed: { actual: 23.7, offset: 0, target: 0 },
//         tool0: { actual: 24.9, offset: 0, target: 0 }
//     }
// }